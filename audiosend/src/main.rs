use std::{str, io::Read, thread, process};
use ws::{connect, Message, Handshake, CloseCode};
use clap::{Arg, App, AppSettings, ArgMatches};
use base64::encode;
use subprocess::Exec;

struct Handler<'a> {
    stream: ws::Sender, 
    userpass: Option<(String, String)>, 
    matches: ArgMatches<'a>,
}

impl<'a> Handler<'a> {
    const MPEGTS_SIZE: usize = 188;
}

impl<'a> ws::Handler for Handler<'a> {
    fn on_open(&mut self, _ : Handshake) -> ws::Result<()> {
        println!("Connected to server");
        let websock = self.stream.clone();
        let ffmpeg = self.matches.value_of("ffmpeg").unwrap().to_string();
        let input_stream = self.matches.value_of("input stream").unwrap().to_string();
        let input_format = self.matches.value_of("input format").map(|s| { s.to_string()});
        let bitrate = self.matches.value_of("bitrate").unwrap().to_string();
        let cbr = self.matches.is_present("cbr");

        let _thr = thread::spawn(move || {
            let mut cmd = Exec::cmd(ffmpeg);
            cmd = cmd.arg("-re");
            if input_format.is_some() {
                cmd = cmd.arg("-f").arg(input_format.unwrap());
            }
            cmd = cmd.arg("-hide_banner");
            cmd = cmd.arg("-i").arg(input_stream);
            cmd = cmd.arg("-c:a").arg("libopus");
            if cbr {
                cmd = cmd.arg("-vbr").arg("off");
            }
            cmd = cmd.arg("-b:a").arg(bitrate);
            cmd = cmd.arg("-f").arg("mpegts").arg("-");
            let mut stream = cmd.stream_stdout().unwrap();
            loop {
                let mut buf = [0; Self::MPEGTS_SIZE];
                match stream.read_exact(&mut buf) {
                    Ok(_) => {
                        websock.send(Message::Binary(buf.to_vec())).unwrap();
                    },
                    Err(_) => {
                        println!("Encoding process exited");
                        process::exit(0);
                    }
                }
            }
        });
        Ok(())
    }

    fn on_message(&mut self, _msg: Message) -> ws::Result<()> {
        Ok(())
    }
    fn on_close(&mut self, _code: CloseCode, _reason: &str) {
        println!("Websocket closed, code: {:?}, reason: {:?}", _code, _reason);
        process::exit(1);
    }

    fn on_error(&mut self, _err: ws::Error) {
        println!("Websocket error: {:?}", _err);
        process::exit(1);
    }

    fn build_request(&mut self, url: &url::Url) -> ws::Result<ws::Request> {
        println!("Building request for URL {}", url);
        let mut req = ws::Request::from_url(url);
        match &mut req {
            Ok(req) => { 
                match &self.userpass {
                    Some((username, password)) => {
                        let encoded_userpass = encode(format!("{}:{}", username, password));
                        let auth_header = format!("Basic {}", encoded_userpass);
                        req.headers_mut().push(("Authorization".to_string(), auth_header.into_bytes()));
                    },
                    _ => {}
                };
            },
            _ => {}
        };
        req
    }
}

fn main() {
    let matches = App::new("Audio send")
        .version("0.1.0")
        .arg(
            Arg::with_name("username")
            .short("U")
            .long("username")
            .required(false)
            .takes_value(true)
            .help("username for HTTP basic auth")
        ).arg(
            Arg::with_name("server url")
            .short("u")
            .long("url")
            .required(true)
            .takes_value(true)
            .help("URL for server (format: ws://host:port/)")
        ).arg(
            Arg::with_name("input stream")
            .short("i")
            .long("input")
            .required(true)
            .takes_value(true)
            .help("input stream (ex: file.mp3)")
        ).arg(
            Arg::with_name("input format")
            .short("f")
            .long("format")
            .required(false)
            .takes_value(true)
            .help("input format (ex: mp3)")
        ).arg(
            Arg::with_name("bitrate")
            .short("b")
            .long("bitrate")
            .required(true)
            .takes_value(true)
            .default_value("64K")
            .help("bitrate (ex: 64K)")
        ).arg(
            Arg::with_name("ffmpeg")
            .short("F")
            .long("ffmpeg")
            .required(true)
            .takes_value(true)
            .default_value("ffmpeg")
            .help("FFMPEG executable name/location")
        ).arg(
            Arg::with_name("cbr")
            .short("c")
            .long("cbr")
            .required(false)
            .takes_value(false)
            .help("Use Constant Bit-Rate")
        ).after_help("EXAMPLE\n    audiosend --url ws://127.0.0.1:8001 -i blah.mp3\n    To stream microphone, use: -f pulse -i default\n")
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    let userpass = match matches.value_of("username") {
        Some(username) => {
            println!("Authenticating as: {}", username);
            Some((username.to_string(), rpassword::read_password_from_tty(Some("Password: ")).unwrap().to_string()))
        },
        _ => None,
    };

    println!("Connecting to server"); 

    let server_url = matches.value_of("server url").unwrap();
    let matches = matches.clone();
    connect(server_url, move |out| {
        Handler {stream:out, userpass: userpass.clone(), matches: matches.clone() }
    }).unwrap();
    println!("Disconnected");
    process::exit(1);
}
