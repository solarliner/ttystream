#!/usr/bin/env python3
import socket
import os
import sys
import traceback
import termios
import atexit
import signal
import asyncio
import websockets
import json

async def send(role, ws, s):
	await ws.send(s)
	print("[" + role + "] send: ", s)

async def recv(role, ws):
	r = await ws.recv()
	print("[" + role + "] recv: ", r)
	return r
	
async def expect(role, ws, w):
	r = await recv(role, ws)
	j = json.loads(r)
	w["ts"] = j["ts"]
	if j != w:
		print("[" + role + "] ERROR: Expected: ", w)
		print("[" + role + "] ERROR: Received: ", json.loads(r))
		raise ValueError("Test failed")
	print("[" + role + "] Received expected message.")

async def presenter():
	async with websockets.connect(uri) as ws:
		try:
			pass
			txt = json.dumps({"type":"hello", "presenter": True})
			await send("PREZ", ws, txt)
			print("[PREZ] connected!")

			#set some metadata (global)
			await send("PREZ", ws, json.dumps({"type":"globalmetadata", "name":"author", "value": "Tester1"}))
			await expect("PREZ", ws, {"type":"globalmetadata", "name":"author", "value": "Tester1"})


		except websockets.exceptions.ConnectionClosed:
			print("[PREZ] disconnected")
		except:
			traceback.print_exc()
			os.kill(os.getpid(), signal.SIGTERM)

uri = sys.argv[1]
print("Connecting...")
asyncio.get_event_loop().run_until_complete(presenter())

    
