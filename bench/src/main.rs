use std::{str, io::Read, thread, process};
use ws::{connect, Message, Handshake, CloseCode, Builder, Settings};
use clap::{Arg, App, AppSettings, ArgMatches};
use base64::encode;
use subprocess::Exec;

struct Handler {
}

impl ws::Handler for Handler {
    fn on_open(&mut self, _ : Handshake) -> ws::Result<()> {
        println!("Websocket connected");
        Ok(())
    }

    fn on_message(&mut self, _msg: Message) -> ws::Result<()> {
        Ok(())
    }
    fn on_close(&mut self, _code: CloseCode, _reason: &str) {
        println!("Websocket closed, code: {:?}, reason: {:?}", _code, _reason);
        process::exit(1);
    }

    fn on_error(&mut self, _err: ws::Error) {
        println!("Websocket error: {:?}", _err);
        process::exit(1);
    }

}

fn main() {
    let matches = App::new("WS bench")
        .version("0.1.0")
        .arg(
            Arg::with_name("server url")
            .short("u")
            .long("url")
            .required(true)
            .takes_value(true)
            .help("URL for server (format: ws://host:port/)")
        )
        .arg(
            Arg::with_name("count")
            .short("c")
            .long("count")
            .required(true)
            .takes_value(true)
            .help("websocket count")
        ).after_help("EXAMPLE\n    bench --url ws://127.0.0.1:8001 -c 1000\n")
        .setting(AppSettings::ArgRequiredElseHelp)
        .get_matches();

    let server_url = url::Url::parse(matches.value_of("server url").unwrap()).unwrap();
    let count = matches.value_of("count").unwrap().parse().unwrap();
    let mut ws = Builder::new().with_settings(Settings{ max_connections: count, ..Settings::default()}).build(|out| {
        Handler { }
    }).unwrap();
    for _ in 0..count {
        ws.connect(server_url.clone()).unwrap();
    }
    ws.run().unwrap();
    process::exit(1);
}
