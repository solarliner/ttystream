#!/usr/bin/env python3
import socket
import os
import sys
import termios
import atexit
import asyncio
import websockets
import json

async def client():
    uri = sys.argv[1]
    async with websockets.connect(uri) as ws:
        try:
            print("Connected!")
            txt = json.dumps({"type":"hello", "presenter": False})
            await ws.send(txt)
            while True:
                data = await ws.recv()
                print(data)
        except websockets.exceptions.ConnectionClosed:
            print("Connection closed")


print("Connecting...")
asyncio.get_event_loop().run_until_complete(client())

    
