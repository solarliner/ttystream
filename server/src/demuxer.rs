use std::{convert::TryInto, io::{Error, ErrorKind, self}};
use log::warn;

pub struct MpegTsDemuxer {
    pesbuf: Vec<u8>,
    pes_remaining: usize,
}

impl MpegTsDemuxer {
    const MPEGTS_SIZE: usize = 188;

    pub fn new() -> Self {
        Self {pesbuf: Vec::new(), pes_remaining: 0 }
    }

    pub fn reset(&mut self) {
        self.pesbuf = Vec::new();
        self.pes_remaining = 0;
    }

    pub fn process_frame<T: FnMut(Vec<u8>) -> ()>(&mut self, buf: &[u8], mut callback:T) -> io::Result<()> {
        if buf.len() != Self::MPEGTS_SIZE {
            warn!("Closing audio source: Invalid MpegTS frame size");
            return Err(Error::new(ErrorKind::Other, "Audio Error"));
        }

        let start = u32::from_be_bytes(buf[0..4].try_into().unwrap());
        let sync = start >> 24;
        if sync != 0x47 {
            warn!("Closing audio source: Invalid SYNC from packet (ensure the stream is in MPEG-TS format)");
            return Err(Error::new(ErrorKind::Other, "Audio Error"));
        }
        let mut payload_offset:usize = 4;

        let pid = (start & 0x1FFF00) >> 8;
        if pid == 0x100 {
            let fl_ac = start & 0x30;
            if fl_ac & 0x20 != 0 {
                /* has Adaptation Field */
                payload_offset += buf[4] as usize + 1; /* add AF length */
            }

            if fl_ac & 0x10 != 0 {
                /* has Payload */
                if self.pes_remaining == 0 {
                    /* start of packetized stream */
                    if payload_offset + 8 >= Self::MPEGTS_SIZE {
                        warn!("Closing audio source: AF length is invalid");
                        return Err(Error::new(ErrorKind::Other, "Audio Error"));
                    }
                    let start_code = u32::from_be_bytes(buf[payload_offset..(payload_offset+4)].try_into().unwrap());
                    let pes_len = u16::from_be_bytes(buf[(payload_offset+4)..(payload_offset+6)].try_into().unwrap());
                    if start_code != 0x1BD {
                        warn!("Closing audio source: Invalid start code received (ensure the stream is encoded with Opus)");
                        return Err(Error::new(ErrorKind::Other, "Audio Error"));
                    }
                    let varlen = buf[payload_offset + 8] as usize;
                    self.pes_remaining = pes_len as usize - 3 - varlen;
                    payload_offset += varlen + 9;
                } else {
                    /* continuation of packetized stream */
                }

                if payload_offset >= Self::MPEGTS_SIZE {
                    warn!("Closing audio source: payload offset points to outside of MPEG-TS frame");
                    return Err(Error::new(ErrorKind::Other, "Audio Error"));
                }
                let payload_size = Self::MPEGTS_SIZE - payload_offset;
                if self.pes_remaining < payload_size {
                    warn!("Closing audio source: Corrupted packetized stream");
                    return Err(Error::new(ErrorKind::Other, "Audio Error"));
                }
                self.pes_remaining -= payload_size;
                self.pesbuf.extend_from_slice(&buf[payload_offset..]);
                if self.pes_remaining == 0 {
                    /* received whole PES, now delimit Opus frames within it */
                    let mut offset: usize = 0;
                    loop {
                        if offset == self.pesbuf.len() {
                            break;
                        } 
                        let prefix = u16::from_be_bytes(self.pesbuf.get(offset..(offset+2)).ok_or(Error::new(ErrorKind::Other,"short read (opus frame prefix)"))?.try_into().unwrap());
                        let mut size_byte:u8 = *self.pesbuf.get(offset + 2).ok_or(Error::new(ErrorKind::Other, "short read (opus frame size 1st byte)"))?;
                        offset += 3;
                        let mut size:usize = size_byte as usize;
                        while size_byte == 0xFF {
                            size_byte = *self.pesbuf.get(offset).ok_or(Error::new(ErrorKind::Other, "short read (opus frame size)"))?;
                            size += size_byte as usize;
                            offset += 1;
                        }

                        /* TODO find official doc on Opus-in-MPEGTS encapsulation (didn't find any) */
                        if prefix == 0x7FF0 {
                            /* is first frame */
                            /* TODO recover Opus skip count ? */
                            offset += 2;
                        } else if prefix == 0x7FE0 {
                            /* standard frame */
                        } else if prefix == 0x7FE8 {
                            offset += 2;
                            /* is last frame */
                        } else {
                            warn!("Closing audio source: Invalid Opus-in-PES frame prefix: {:X}", prefix);
                            return Err(Error::new(ErrorKind::Other, "Audio Error"));
                        }
                        if offset + size as usize > self.pesbuf.len() {
                            warn!("Closing audio source: short read (opus frame)");
                            return Err(Error::new(ErrorKind::Other, "Audio Error"));
                        }
                        callback(self.pesbuf[offset..(offset + size as usize)].to_vec());
                        offset += size as usize;
                    }

                    self.pesbuf.clear();
                }
            }
        }
        Ok(())
    }
}

