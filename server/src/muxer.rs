use std::io::Write;

pub struct Muxer {
    block_count: u64,
    cluster_count: u64,
    buffer: Vec<u8>,
}

/* generic ebml stuff */
const EBML_HEADER: u64 = 0x1A45DFA3;
const EBML_VERSION: u64 = 0x4286;
const EBML_READER_VERSION: u64 = 0x42F7;
const EBML_MAX_ID_LENGTH : u64 = 0x42F2;
const EBML_MAX_SIZE_LENGTH : u64 = 0x42F3;
const EBML_DOCTYPE: u64 = 0x4282;
const EBML_DOCTYPE_VERSION: u64 = 0x4287;
const EBML_DOCTYPE_READER_VERSION: u64 = 0x4285;

const EBML_SIZE_UNKNOWN:u64 = 0xFFFFFFFFFFFFFF;

/* matroska stuff */
const MKV_SEGMENT: u64 = 0x18538067;
const MKV_INFO: u64 = 0x1549A966;
const MKV_TS_SCALE:u64 = 0x2AD7B1;
const MKV_MUXER:u64 = 0x4D80;
const MKV_WRITER:u64 = 0x5741;

const MKV_TRACKS:u64 = 0x1654AE6B;
const MKV_TRACK_ENTRY:u64 = 0xAE;
const MKV_TRACK_NUMBER:u64 = 0xD7;
const MKV_TRACK_UID:u64 = 0x73C5;
const MKV_TRACK_TYPE:u64 = 0x83;

const MKV_CODEC_DELAY:u64 = 0x56AA;
const MKV_SEEK_PRE_ROLL:u64 = 0x56BB;
const MKV_CODEC_ID:u64 = 0x86;
const MKV_CODEC_PRIVATE:u64 = 0x63A2;

const MKV_AUDIO_SETTINGS:u64 = 0xE1;
const MKV_AUDIO_FREQUENCY:u64 = 0xB5;
const MKV_AUDIO_CHANNELS:u64 = 0x9F;
const MKV_AUDIO_BIT_DEPTH:u64 = 0x6264;

const MKV_CLUSTER: u64 = 0x1F43B675;
const MKV_TIMESTAMP: u64 = 0xE7;
const MKV_SIMPLE_BLOCK: u64 = 0xA3;

const TRACK_TYPE_AUDIO: u8 = 2;

const BLOCKS_PER_CLUSTER: u64 = 50; // 1sec

const OPUS_FRAME_DURATION:u64 = 20;

impl Muxer {
    pub fn new() -> Self {
        Self { block_count: 0, cluster_count: 0, buffer: Vec::new() }
    }

    pub fn reset(&mut self) {
        self.buffer = Vec::new();
        self.block_count = 0;
        self.cluster_count = 0;
    }

    pub fn get_header(&self, output: &mut Vec<u8>) {
        let pre_skip:u16 = 0x138; /* TODO */

        /* EBML header */

        write_id(output, EBML_HEADER);
        write_vint(output, 31); // size

        write_id(output, EBML_VERSION);
        write_vint(output, 1); // size
        write_byte(output, 1); // data (version: 1)

        write_id(output, EBML_READER_VERSION);
        write_vint(output, 1); // size
        write_byte(output, 1); // data (version: 1)
        
        write_id(output, EBML_MAX_ID_LENGTH);
        write_vint(output, 1); // size
        write_byte(output, 4); 

        write_id(output, EBML_MAX_SIZE_LENGTH);
        write_vint(output, 1); // size
        write_byte(output, 8);

        write_id(output, EBML_DOCTYPE);
        write_vint(output, 4); // size
        output.write_all(b"webm").unwrap();

        write_id(output, EBML_DOCTYPE_VERSION);
        write_vint(output, 1); // size
        write_byte(output, 4); 

        write_id(output, EBML_DOCTYPE_READER_VERSION);
        write_vint(output, 1); // size
        write_byte(output, 2); 


        /* Matroska header */

        write_id(output, MKV_SEGMENT);
        write_vint(output, EBML_SIZE_UNKNOWN);

        write_id(output, MKV_INFO);
        let name = b"klemux v0.1";
        write_vint(output, 13 + name.len() as u64*2);

        write_id(output, MKV_TS_SCALE);
        write_vint(output, 3);
        output.write_all(&[0x0F, 0x42, 0x40]).unwrap(); // TS scale: milliseconds

        write_id(output, MKV_MUXER);
        write_vint(output, name.len() as u64);
        output.write_all(name).unwrap();

        write_id(output, MKV_WRITER);
        write_vint(output, name.len() as u64);
        output.write_all(name).unwrap();

        write_id(output, MKV_TRACKS);
        write_vint(output, 76);

        write_id(output, MKV_TRACK_ENTRY);
        write_vint(output, 74);

        write_id(output, MKV_TRACK_NUMBER);
        write_vint(output, 1);
        write_byte(output, 1); /* track number 1 */

        write_id(output, MKV_TRACK_UID);
        write_vint(output, 7);
        output.write_all(&[0xDE, 0xAD, 0xBE, 0xEF, 0xAA, 0xBB, 0xCC]).unwrap();

        write_id(output, MKV_TRACK_TYPE);
        write_vint(output, 1);
        write_byte(output, TRACK_TYPE_AUDIO);

        write_id(output, MKV_CODEC_DELAY);
        write_vint(output, 3);
        output.write_all(&[0x63, 0x2E, 0xA0]).unwrap(); // Opus Codec Delay: 6.5msec (value taken from libwebm)

        
        write_id(output, MKV_SEEK_PRE_ROLL);
        write_vint(output, 4);
        output.write_all(&[0x4, 0xC4, 0xB4, 0x00]).unwrap(); // Opus Codec seek Pre-roll: 8msec (value taken from libwebm)


        write_id(output, MKV_CODEC_ID);
        write_vint(output, 6);
        output.write_all(b"A_OPUS").unwrap();

        write_id(output, MKV_CODEC_PRIVATE);
        write_vint(output, 19);
        output.write_all(b"OpusHead").unwrap();
        write_byte(output, 0x01); // opus version
        write_byte(output, 2); // channel count
        write_u16_le(output, pre_skip);
        write_u32_le(output, 48000); // sampling rate (always 48khz with Opus)
        write_u16_le(output, 0); // output gain 
        write_byte(output, 0); // mapping family

        write_id(output, MKV_AUDIO_SETTINGS);
        write_vint(output, 13);

        write_id(output, MKV_AUDIO_FREQUENCY);
        write_vint(output, 4);
        output.write_all(&[0x47, 0x3B, 0x80, 0x00]).unwrap(); // 48k (encoded as float)


        write_id(output, MKV_AUDIO_CHANNELS);
        write_vint(output, 1);
        write_byte(output, 2); // 2 channels

        write_id(output, MKV_AUDIO_BIT_DEPTH);
        write_vint(output, 1);
        write_byte(output, 16); // 16 bits per sample
    }

    pub fn take_cluster(&mut self) -> Vec<u8> {
        std::mem::take(&mut self.buffer)
    }


    /* returns true if frame is a new chunk */
    pub fn process_frame(&mut self, raw_frame: Vec<u8>) {
        if self.block_count == 0 {
            let offset = self.next_cluster_ts();
            Self::write_cluster_header(&mut self.buffer, offset);
            self.cluster_count += 1;
        }

        let output = &mut self.buffer;

        let size = raw_frame.len();

        write_id(output, MKV_SIMPLE_BLOCK);
        write_vint(output, size as u64 + 4);
        write_vint(output, 1); // track number
        write_u16_be(output, (self.block_count * OPUS_FRAME_DURATION) as u16);
        let flags:u8 = 0x80; // keyframe
        write_byte(output, flags);

        output.write_all(&raw_frame).unwrap();

        self.block_count = (self.block_count + 1) % BLOCKS_PER_CLUSTER;
    }

    /* the 4 following functions are meant to be called *before* process_frame() */

    pub fn is_cluster_start(&self) -> bool {
        self.block_count == 0
    }

    pub fn is_cluster_end(&self) -> bool {
        self.block_count == (BLOCKS_PER_CLUSTER - 1) 
    }

    pub fn is_first_cluster(&self) -> bool {
        self.cluster_count == 0 || (self.cluster_count == 1 && self.block_count > 0)
    }

    pub fn next_cluster_ts(&self) -> u64 {
        self.cluster_count * OPUS_FRAME_DURATION * BLOCKS_PER_CLUSTER
        
    }

    fn write_cluster_header(output: &mut Vec<u8>,ts: u64) {
        write_id(output, MKV_CLUSTER); // 4 bytes
        write_vint(output, EBML_SIZE_UNKNOWN); // 8 bytes

        write_id(output, MKV_TIMESTAMP); // 1 byte
        let tmp = ts.to_be_bytes();

        /* always write TS on 8 bytes to facilitate patching by client */
        write_vint(output, 8); // 1 byte
        output.write_all(&tmp[0..8]).unwrap();
    }

    pub fn reloc_offset(&self) -> usize {
        14 /* position of timestamp in cluster header */
    }

    pub fn reloc_size(&self) -> usize {
        8 /* size of timestamp in cluster header */
    }
}


fn get_raw_size(i: u64)  -> usize {
    if i < 0x100 {
        return 1; }
    if i < 0x10000 { 
        return 2; }
    if i < 0x1000000 { 
        return 3; }
    if i < 0x100000000 {
        return 4; }
    if i < 0x10000000000 {
        return 5; }
    if i < 0x1000000000000 {
        return 6; }
    if i < 0x100000000000000 {
        return 7; }
    return 8;
}

fn get_coded_size(i: u64)  -> usize {
    return get_raw_size((i + 1) << 1);
}

fn write_id(output: &mut Vec<u8>, id: u64) {
    let buf = id.to_be_bytes();
    let size = get_raw_size(id);
    output.write_all(&buf[(8 - size)..8]).unwrap();
}

fn write_vint(output: &mut Vec<u8>, v: u64) {
    let size = get_coded_size(v);
    let coded: u64 = (1 << 7*size) | v;
    let buf = coded.to_be_bytes();
    output.write_all(&buf[(8 - size)..8]).unwrap();
}

fn write_byte(output: &mut Vec<u8>,v: u8) {
    output.write_all(&[v]).unwrap();
}

fn write_u16_le(output: &mut Vec<u8>,v: u16) {
    let buf = v.to_le_bytes();
    output.write_all(&buf).unwrap();
}

fn write_u16_be(output: &mut Vec<u8>,v: u16) {
    let buf = v.to_be_bytes();
    output.write_all(&buf).unwrap();
}

fn write_u32_le(output: &mut Vec<u8>,v: u32) {
    let buf = v.to_le_bytes();
    output.write_all(&buf).unwrap();
}
