use chrono::{DateTime, Utc};

pub struct PlugMessage {
    pub chan_id: u32,
    pub data: String,
}

pub fn now() -> Option<DateTime<Utc> > {
    return Some(Utc::now())

}
