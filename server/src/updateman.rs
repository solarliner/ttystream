use std::{thread, sync::{Arc, Mutex, mpsc::Receiver}, time, str, io::{self, Error, ErrorKind}};
use terminal_emulator::{term::{Term, SizeInfo, cell::Flags}, ansi::{Color, NamedColor}, Processor};
use log::debug;

pub struct UpdateMan { 
    /* Communication */
    /** Channel receiving data from server */
    receiver: Receiver<Vec<u8> >,

    /** State shared with server */
    plug_state: Arc<Mutex<PlugState> >,

    /* Callback to send data to server */
    sender: Box<dyn Fn(String) -> () + Send>,

    /* helper */
    processor: Processor,

    /* Tuning */
    tick_delay: u32,
}

struct Intensity {}
impl Intensity {
    const BOLD:u32 = 1;
    const DIM:u32 = 2;
    const NORMAL:u32 = 22;
}

pub struct PlugState {
    term: Term,
}

struct AttrState {
    intensity: u32, /* dim, normal, bold */
    flags: Flags, /* underline, inverse */ 
    bg: NamedColor,
    fg: NamedColor,
    reset_done: bool, /* flag indicating an attribute reset was done */
}

impl AttrState {
    fn new() -> AttrState {
        AttrState { 
            intensity: Intensity::NORMAL,
            flags: Flags::empty(),
            bg: NamedColor::Background,
            fg: NamedColor::Foreground,
            reset_done: true,
        }
    }

    fn handle_color(&mut self, new_color: &Color, is_bg: bool, intensity: u32, attributes: &mut Vec<u8>) -> u32 {
        let current_named = if is_bg { &mut self.bg } else { &mut self.fg };

        /* Only support named color for now, others are mapped to default */
        let new_named = match *new_color {
            Color::Named(color) => color,
            Color::Spec(_) => if is_bg { NamedColor::Background} else { NamedColor::Foreground},
            Color::Indexed(_) => if is_bg { NamedColor::Background} else { NamedColor::Foreground},
        };

        /* If color set to default, only way to reset_done it it to reset_done all attributes */
        let default = if is_bg { NamedColor::Background} else { NamedColor::Foreground};
        if !self.reset_done && new_named == default && *current_named != default {
            attributes.extend(b"0;");
            self.reset_done = true;
        }

        let mut intensity = intensity;
        let code_offset = if is_bg { 40 } else { 30 };

        /* Send code only if color has changed, or if forced by attribute reset_done */
        
        if self.reset_done || *current_named != new_named {
            if new_named >= NamedColor::Black && new_named <= NamedColor::White {
                attributes.extend(format!("{};", (new_named as usize) + code_offset).as_bytes());
            }
            if new_named >= NamedColor::BrightBlack && new_named <= NamedColor::BrightWhite {
                attributes.extend(format!("{};", (new_named as usize) + code_offset - 8).as_bytes());
                intensity = Intensity::BOLD;
            }
            if new_named >= NamedColor::DimBlack && new_named <= NamedColor::DimWhite {
                attributes.extend(format!("{};", (new_named as usize) + code_offset - 16).as_bytes());
                intensity = Intensity::DIM;
            }
        }

        *current_named = new_named;
        return intensity;
    }

    fn handle_attributes(&mut self, flags: &Flags, bg: &Color, fg: &Color, output: &mut Vec<u8>) {
        let mut attributes: Vec<u8> = Vec::new();

        /* Color can affect intensity (i.e. BrightXXX / DimXXX colors) */ 
        let mut intensity = self.handle_color(bg, true, Intensity::NORMAL, &mut attributes);
        intensity = self.handle_color(fg, false, intensity, &mut attributes);

        /* Intensity specified in flags take precedence over intensity specified in the color */
        if *flags & Flags::BOLD != Flags::empty() {
            intensity = Intensity::BOLD;
        } else if *flags & Flags::DIM != Flags::empty() {
            intensity = Intensity::DIM;
        }

        /* send intensity */
        if self.reset_done || intensity != self.intensity {
            attributes.extend(format!("{};", intensity).as_bytes());
        }
        self.intensity = intensity;

        /* send flags (except intensity) */
        if self.reset_done || *flags & Flags::INVERSE != self.flags & Flags::INVERSE {
            attributes.extend((if *flags & Flags::INVERSE != Flags::empty() { "7;" } else { "27;" }).as_bytes());
        }

        if self.reset_done || *flags & Flags::UNDERLINE != self.flags & Flags::UNDERLINE {
            attributes.extend((if *flags & Flags::UNDERLINE != Flags::empty() { "4;" } else { "24;" }).as_bytes());
        }
        self.flags = *flags;

        if attributes.len() > 0 {
            output.extend(b"\x1B[");
            output.extend(&attributes[0..(attributes.len() - 1)]);
            output.extend(b"m");
        }
    }
}

impl PlugState {
    fn new(term: Term) -> PlugState {
        PlugState { 
            term: term,
        }
    }

    pub fn resize(&mut self, width: u8, height: u8) {
        self.term.resize(&SizeInfo {width: width as f32, height: height as f32, cell_height: 1.0, cell_width: 1.0, padding_x: 0.0, padding_y:0.0, dpr: 1.0 });
    }

    pub fn get_redraw(&mut self, output: &mut Vec<u8>) {
        output.extend("\x1B[H\x1B[2J".as_bytes()); /* clear screen */
        let mut buf = [0; 16]; 
        let mut prev_row = 0;
        let mut prev_col = 0;
        let end_attributes = self.term.cursor().attributes();

        let mut redraw_state = AttrState::new();

        for cell in self.term.renderable_cells() {
            if cell.bg == Color::Named(NamedColor::Cursor) {
                continue;
            }
            let row = cell.line.0;
            let col = cell.column.0;

            /* We do this because terminal-emulator lib omits whitespace cells */
            if prev_row != row || prev_col != col {
                /* We assume that every skipped character has default attribute */
                output.extend(b"\x1B[0m");
                redraw_state.reset_done = true;
            }

            if prev_row != row {
                for _ in prev_row..row {
                    output.extend(b"\r\n");
                }
                prev_col = 0;
            } 
            if prev_col != col {
                for _ in prev_col..col {
                    output.extend(b" ");
                }
            }
            prev_row = row;

            /* send attributes */
            redraw_state.handle_attributes(&cell.flags, &cell.bg, &cell.fg, output);

            /* send character */
            let mut first_char = false;
            for ch in cell.chars.iter() {
                if *ch != ' ' || !first_char {
                    first_char = true;
                    prev_col = col + 1;
                    let r = ch.encode_utf8(&mut buf);
                    for i in 0..(r.len()) {
                        output.push(buf[i]);
                    }
                }
            }
            redraw_state.reset_done = false;
        }
        /* Send end (current) attributes */
        redraw_state.handle_attributes(&end_attributes.flags, &end_attributes.bg, &end_attributes.fg, output);

        /* Send cursor position */
        let cursor = self.term.cursor().point;
        output.extend(format!("\x1B[{};{}H", cursor.line + 1, cursor.col + 1).as_bytes());
    }
}

impl UpdateMan {
    pub fn new(receiver: Receiver<Vec<u8> >, sender: Box<dyn Fn(String) -> () + Send>, width: u8, height: u8, tick_delay: u32) -> UpdateMan {
        UpdateMan {
            receiver: receiver,
            plug_state: Arc::new(Mutex::new(PlugState::new(Term::new(SizeInfo {width: width as f32, height: height as f32, cell_height: 1.0, cell_width: 1.0, padding_x: 0.0, padding_y:0.0, dpr: 1.0 })))),
            sender: sender,
            processor: Processor::new(),
            tick_delay: tick_delay
        }
    }

    pub fn get_state(&self) -> Arc<Mutex<PlugState> > {
        self.plug_state.clone() 
    }

    fn main_loop(&mut self) -> ws::Result<()> {
        loop {
            thread::sleep(time::Duration::from_millis(self.tick_delay as u64 * 5));
            /* Receiving data from daemon (threw the server) */
            let mut data = self.receiver.recv().or(Err(Error::new(ErrorKind::Other, "Channel was closed by someone")))?;

            /* flush receiver channel */
            loop {
                match self.receiver.try_recv() {
                    Ok(d) =>  {
                        data.extend(d);
                    },
                    _ => {
                        break;
                    }
                }
            }
            /* end of receive */
            let mut plug_state = self.plug_state.lock().unwrap();

            for byte in &data {
                self.processor.advance(&mut plug_state.term, *byte, &mut io::sink());
            }

            if data.len() > 2048 {
                let mut redraw = Vec::new();
                plug_state.get_redraw(&mut redraw);
                if redraw.len() < data.len() {
                    debug!("Sending redraw ({} bytes instead of {})", redraw.len(), data.len());
                    data = redraw;
                }
            }
            match str::from_utf8(&data) {
                Ok(content) => {
                    (*self.sender)(content.to_string());
                },
                e => {
                    debug!("UpdateMan: Failed to send update, invalid utf8 from daemon, str={:?} err={:?}", data, e);
                },
            }; 
        }
    }

    pub fn launch(mut self) {
        let _ = thread::spawn(move || {
            let e = self.main_loop();
            debug!("UpdateMan thread quitting because: {:?}", e);
        });
    }
}
