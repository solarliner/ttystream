use std::{time, process::exit, thread, sync::{Arc, Mutex,  mpsc::{channel, Receiver, Sender}}, collections::HashMap, io::{Error, ErrorKind}, str};
use ws::{Settings, Builder, Message, CloseCode};
use uuid::Uuid;
use log::{info, warn, error, debug};
use chrono::{DateTime, Utc, Duration};
use protocol::*;
use crate::{shared::{now, PlugMessage}, updateman::{PlugState, UpdateMan}, muxer::Muxer, demuxer::MpegTsDemuxer};
use xz2::stream::{Stream, LzmaOptions, Action, Status};

trait JSONBuffer {
    fn take(&mut self, compress: bool) -> Option<ws::Message>;
}

impl JSONBuffer for Vec<JSONMessage> {
    fn take(&mut self, compress: bool) -> Option<ws::Message> {
        if self.len() == 0 {
            return None
        }
        let txt = serde_json::to_string(&self).unwrap();
        self.clear();
        if compress {
//            println!("encoding {} bytes", txt.len());
            let mut encoder = Stream::new_lzma_encoder(&LzmaOptions::new_preset(9).unwrap()).unwrap();
            let mut output = Vec::new();
            output.push(protocol::BIN_LZMA);
            output.reserve(txt.len());
            let result = encoder.process_vec(txt.as_bytes(), &mut output, Action::Finish);
            match result {
                Ok(Status::StreamEnd) => {
                    Some(Message::binary(output.clone()))
                },
                _ => {
                    Some(Message::text(txt))
                }
            }
        } else {
            Some(Message::text(txt))
        }
    }
}

struct Chan {
    scene: u32,
    initdata: ChanInitData,
    metadata: HashMap<String, String>,
    owned_by: uuid::Uuid,

    /* Communication with plugin */
    state: Option<Arc<Mutex<PlugState> > >,
    sender: Option<Sender<Vec<u8> > >,
}


pub enum Event {
    ViewServer(ws::Sender),
    PrezServer(ws::Sender),
    Plugin(PlugMessage),
    Open(uuid::Uuid, ws::Sender, bool),
    Client(uuid::Uuid, JSONMessage),
    ClientBin(uuid::Uuid, Vec<u8>),
    HeartBeat,
    Close(uuid::Uuid),
}

fn fail<T: Into<String> >(s: T) -> ws::Error {
    From::from(Box::new(Error::new(ErrorKind::Other, s.into())))
}

struct SharedState {
    focused: Option<u32>,
    metadata: HashMap<String, String>,
    chan_list: HashMap<u32, Chan>,
    next_id: u32,
    event_sender: Sender<Event>, 
    all_viewers: Option<ws::Sender>,
    all_presenters: Option<ws::Sender>,
    buffer: Vec<JSONMessage>,
    rl_last: DateTime<Utc>,
    tick_delay: u32,
}

impl SharedState {
    fn new(event_sender: Sender<Event>, tick_delay: u32,) -> SharedState {
        SharedState { 
            focused: None,
            metadata: HashMap::new(),
            chan_list: HashMap::new(),
            next_id: 0,
            event_sender: event_sender,
            all_viewers: None,
            all_presenters: None,
            buffer: Vec::new(),
            rl_last: Utc::now(),
            tick_delay: tick_delay,
        } 
    }

    fn generate_chan_id(&mut self) -> u32 {
        let id = self.next_id;
        self.next_id += 1;
        id
    }

    fn new_tty(&self, width: u8, height: u8, chan_id: u32, _receiver: Receiver<Vec<u8> > ) -> ws::Result<Arc<Mutex<PlugState> > >{
        let plug_sender = self.event_sender.clone();
        let f = move |data:String| {
            let msg = Event::Plugin(PlugMessage { 
                chan_id: chan_id,
                data: data
            });

            plug_sender.send(msg).expect("Sending from Plugin to Server failed");
        };
        let update_man = UpdateMan::new(_receiver, Box::new(f), width, height, self.tick_delay);
        let plug_state = update_man.get_state();
        update_man.launch();
        return Ok(plug_state);
    }

    fn kill_tty(&mut self, chan_id: u32) {
        if self.chan_list.contains_key(&chan_id) {
            match self.chan_list.get(&chan_id).unwrap().sender.as_ref() {
                Some(sender) => {
                    debug!("Closing communication with plugin for channel {}", chan_id);
                    drop(sender);
                },
                _ => {}
            }
        }
    }
    fn ensure_unfocused(&mut self, chan_id: u32) -> Option<u32> {
        match self.focused {
            Some(focus) if focus == chan_id => {
                self.focused = self.chan_list.keys().nth(0).cloned();
                self.focused
            },
            _ => None, 
        }
    }

    fn del_channel(&mut self, chan_id: u32) -> Option<u32>{
        info!("Closing channel {}", chan_id);
        self.chan_list.remove(&chan_id);
        let new_focus = self.ensure_unfocused(chan_id);
        self.kill_tty(chan_id);
        new_focus
    }

    fn new_channel(&mut self, chan_id: u32, chan :Chan) {
        info!("Opening new channel {} owned by {}", chan_id, chan.owned_by);
        self.chan_list.insert(chan_id, chan);
    }
    fn is_buffer_empty(&self) -> bool {
        self.buffer.len() == 0
    }

    fn flush_json(&mut self) {
        match self.buffer.take(true) {
            Some(msg) => {
                for stream in self.all_viewers.iter() {
                    let _ = stream.broadcast(msg.clone());
                }
            },
            None => {},
        }
    }


    fn broadcast_json_viewers(&mut self, json_msg: &JSONMessage) {
        self.buffer.push(json_msg.clone());
    }

    fn broadcast_json(&mut self, json_msg: &JSONMessage) {
        self.broadcast_json_viewers(json_msg);

        let mut tmp = Vec::new();
        tmp.push(json_msg.clone());
        let msg = tmp.take(false).unwrap();
        for stream in self.all_presenters.iter() {
            let _ = stream.broadcast(msg.clone());
        }
    }
}

struct Client {
    my_id: uuid::Uuid,
    me: ws::Sender,
    presenter: bool,
    audio_active: bool,
    audio_source: bool,
    closing: bool,
    buffer: Vec<JSONMessage>,
}

impl Client {
    fn flush_json(&mut self) {
        match self.buffer.take(false) {
            Some(msg) => {
                let _ = self.me.send(msg);
            },
            None => {},
        }
    }

    fn send_json(&mut self, json_msg: &JSONMessage) {
        self.buffer.push(json_msg.clone());
        if self.presenter {
            self.flush_json();
        }
    }

    fn new(id: uuid::Uuid, stream: ws::Sender, forbid_presenter: bool) -> Client {
        Client { my_id: id, me: stream, presenter: !forbid_presenter, audio_active: false, audio_source: false, closing: false, buffer: Vec::new() }
    }

    fn on_close(&mut self, shared_state: &mut SharedState) {
        if self.presenter {
            debug!("Client {} was presenter, removing owned channels...", self.my_id);
            let old_focus = shared_state.focused;
            let mut to_remove = Vec::new();

            for (chan_id, chan) in shared_state.chan_list.iter() {
                if chan.owned_by == self.my_id {
                    match chan.state { /* remove only tty chans for now */
                        Some(_) => { to_remove.push(*chan_id) },
                        _ => {}
                    };
                }
            }
            for chan_id in to_remove.iter() {
                shared_state.del_channel(*chan_id);
            }
            if old_focus != shared_state.focused {
                let json_msg = JSONMessage::focus {
                    ts: now(),
                    id: shared_state.focused,
                };
                shared_state.broadcast_json(&json_msg);
            }
            for chan_id in to_remove.iter() {
                let json_msg = JSONMessage::control {
                    ts: now(),
                    ctl: CtrlMessage::delete {
                        id: *chan_id,
                        reason: Some("Presenter disconnected".to_string()),
                    },
                };
                shared_state.broadcast_json(&json_msg);
            }
        }
    }

    fn update_state(&mut self, shared_state: &mut SharedState, msg: JSONMessage) -> ws::Result<()>  {
        match msg { 
            JSONMessage::audio { .. } => {
                Err(fail("Invalid \"audio\" message sent by client"))
            },
            JSONMessage::control {ctl, ts: _ts} => {
                match &ctl {
                    CtrlMessage::create {id, scene, init} => {
                        let id = id.unwrap();
                        let mut plug_state = None;
                        let mut sender = None;
                        let _result:ws::Result<()> = match init {
                            /* special handling for tty because of the daemon */
                            ChanInitData::tty {remote: _, cols, rows} => {
                                let (_sender,receiver) = channel();
                                sender = Some(_sender);
                                plug_state = Some(shared_state.new_tty(*cols, *rows, id, receiver)?);
                                Ok(())
                            },
                            _ => { Ok(()) },
                        };
                        _result?;
                        shared_state.new_channel(id, Chan { owned_by: self.my_id, sender: sender, scene: *scene, initdata: init.clone(), state: plug_state, metadata: HashMap::new()});
                        let json_msg = JSONMessage::created {
                            ts: now(),
                            id: id,
                        };
                        let txt = serde_json::to_string(&json_msg).unwrap();
                        let _ = self.me.send(Message::text(&txt));
                        Ok(())
                    },
                    CtrlMessage::delete { id, .. } => {
                        match shared_state.del_channel(*id) {
                            Some(new_focus) => {
                                let json_msg = JSONMessage::focus {
                                    ts: now(),
                                    id: Some(new_focus)
                                };
                                shared_state.broadcast_json(&json_msg);
                            },
                            None => {},
                        }
                        Ok(())
                    },
                    CtrlMessage::r#move {scene, id} => {
                        shared_state.chan_list.get_mut(&id)
                            .ok_or(fail(format!("Attempted to change metadata on nonexistent channel {}", id)))?
                            .scene = *scene;
                        Ok(())
                    },
                }
            },

            JSONMessage::focus { id, ts: _ts } => {
                shared_state.focused = id;
                Ok(())
            },

            JSONMessage::globalmetadata {ts: _ts, name, value} => {
                shared_state.metadata.insert(name.clone(), value.clone());
                Ok(())
            },

            JSONMessage::channel {id, data, ts: _ts} => {
                match data.clone() {
                    ChanMessage::data { data } => {
                        match data {
                            Data::action {content}=> {
                                let opt = shared_state.chan_list.get_mut(&id).ok_or(fail(format!("Attempted to send content to nonexistent channel {}", id)))?;
                                let sender = opt.sender.as_ref().ok_or(fail(format!("Attempted to send content to non-tty channel {}", id)))?;
                                sender.send(content.into_bytes()).expect("Sending from Server to Plugin failed");
                                Ok(())
                            },
                            Data::resize {cols, rows} => {
                                let opt = shared_state.chan_list.get_mut(&id).ok_or(fail(format!("Attempted to resize a nonexistent channel {}", id)))?;
                                let state = opt.state.as_ref().ok_or(fail(format!("Attempted to resize a non-tty channel {}", id)))?;
                                state.lock().unwrap().resize(cols, rows);
                                match &mut opt.initdata {
                                    ChanInitData::tty { cols : r_cols, rows: r_rows, .. } => {
                                        *r_cols = cols;
                                        *r_rows = rows;
                                    },
                                    _ => {
                                        panic!("BUG: Channel {} State is TTY, but InitialData is PDF... wtf?", id);
                                    }
                                }
                                Ok(())
                            },
                            Data::page { page } => {
                                /* Change state to remember current page for new clients */
                                let chan = shared_state.chan_list.get_mut(&id)
                                    .ok_or(fail(format!("Attempted to send data to inexistent channel {}", id)))?;
                                match &mut chan.initdata {
                                    ChanInitData::pdf { initial_page, .. } =>  {
                                        *initial_page = page;
                                        Ok(())
                                    },
                                    _ => {
                                        Err(fail(format!("Attempted to change page for non-PDF channel {}", id)))
                                    }
                                }
                            },
                            Data::chat {..} => {
                                /* Chat history not supported yet, just broadcast "new" msgs */
                                Ok(())
                            },
                        }
                    },
                    ChanMessage::metadata { name, value } => {
                        shared_state.chan_list.get_mut(&id)
                            .ok_or(fail(format!("Attempted to change metadata on nonexistent channel {}", id)))?
                            .metadata.insert(name, value);
                        Ok(())
                    },
                }
            },
            JSONMessage::created{ .. } => {
                Err(fail("Invalid \"created\" message sent by client"))
            },

            JSONMessage::sync{ .. } => {
                Err(fail("Invalid \"sync\" message sent by client"))
            },

            JSONMessage::heartbeat{ .. } => {
                Err(fail("Invalid \"heartbeat\" message sent by client"))
            },
            JSONMessage::initdone{ .. } => {
                Err(fail("Invalid \"initdone\" message sent by client"))
            },
            JSONMessage::pointer{ .. } => { 
                Ok(())
            }

            JSONMessage::hello { .. } => {
                /* should never be reached because JSONMessage::hello is handled in on_message() */ 
                panic!("INTERNAL ERROR: Received JSONMessage::hello in update_state (should not happen)");
            }
        }
    }

    fn on_open(&mut self, shared_state :&mut SharedState) {
        debug!("Sending global metadata for new client");

        let json_msg = JSONMessage::sync {
            ts: now(),
        };

        self.send_json(&json_msg);
        self.flush_json();

        /* Global metadata */
        for (name, value) in shared_state.metadata.iter() {
            let json_msg = JSONMessage::globalmetadata {
                ts: now(),
                name: name.to_string(),
                value: value.to_string(),
            };
            self.send_json(&json_msg);
        }


        /* Channel list */
        for (chan_id, chan) in shared_state.chan_list.iter() {
            debug!("Sending info for channel {} to new client", *chan_id);
            /* Channel creation w/ init data */
            let json_msg = JSONMessage::control {
                ts: now(),
                ctl: CtrlMessage::create {
                    scene: chan.scene,
                    id: Some(*chan_id),
                    init: chan.initdata.clone(),
                },
            };
            self.send_json(&json_msg);

            for (name, value) in chan.metadata.iter() {
                let json_msg = JSONMessage::channel {
                    ts: now(),
                    id: *chan_id,
                    data: ChanMessage::metadata {
                        name: name.to_string(),
                        value: value.to_string(),
                    }
                };
               self.send_json(&json_msg);
            }

            /* Channel terminal state if applicable */
            match &chan.state {
                Some(plug_state) => {
                    debug!("Sending initial terminal state for this channel");

                    let mut redraw = Vec::new();
                    plug_state.lock().unwrap().get_redraw(&mut redraw);
                    let json_msg = JSONMessage::channel {
                        ts: now(),
                        id: *chan_id,
                        data: ChanMessage::data {
                            data: Data::action {
                                content: str::from_utf8(&redraw).unwrap().to_string(),
                            }
                        },
                    };
                    self.send_json(&json_msg);
                },
                _ => {
                    debug!("Not sending initial terminal state because it is a non-tty channel");
                }
            }
        }

        /* Focused channel */ 
        match shared_state.focused {
            None => {},
            focused => {
                let json_msg = JSONMessage::focus {
                    ts: now(),
                    id: focused
                };
                self.send_json(&json_msg);
            }
        }

        self.send_json(&JSONMessage::initdone { ts: now() });
    }

    fn on_json(&mut self, shared_state :&mut SharedState, json_msg: &mut JSONMessage) -> ws::Result<()> {
        match json_msg {
            JSONMessage::hello { .. } => {
                Ok(())
            },
            mut msg => {
                if self.presenter {
                    /*
                     * Broadcast received message to all, with these exceptions/changes:
                     * - the ts is rewritten to now() (we do not trust the client)
                     * - in case of channel creation, the id is generated
                     * - tty action messages are not broadcasted here because they are handled by the
                     * Channel Plugin (UpdateMan).
                     */

                    let mut broadcast = true;
                    match &mut msg {
                        JSONMessage::control { ts, ctl } => { 
                            match ctl {
                                CtrlMessage::create {id, ..} => {
                                    *id = Some(shared_state.generate_chan_id());
                                },
                                _ => {},
                            }
                            *ts = now();
                        },
                        JSONMessage::focus { ts, .. } => { *ts = now() },
                        JSONMessage::channel { ts, data, .. } => { 
                            match data {
                                ChanMessage::data { data: Data::action { ..}, .. } => {
                                    broadcast = false;
                                },
                                _ => {},
                            }
                            *ts = now(); 
                        },
                        JSONMessage::pointer { ts, .. } => { 
                            let now = Utc::now();
                            if (now - shared_state.rl_last).num_milliseconds() < (shared_state.tick_delay as i64) {
                                broadcast = false;
                            } else {
                                shared_state.rl_last = now;
                                *ts = Some(now);
                            }
                        },
                        JSONMessage::globalmetadata { ts, .. } => { *ts = now() },
                        _ => {},
                    };

                    self.update_state(shared_state, msg.clone())?;
                    if broadcast {
                        shared_state.broadcast_json(&msg);
                    }
                    Ok(())
                } else {
                    Err(fail(format!("Viewer {} attempted to change state (Permission denied", self.my_id)))
                }
            },
        }
    }
}

pub struct EventListener {
    event_receiver: Receiver<Event>,
    event_sender: Sender<Event>,
    clients: HashMap<uuid::Uuid, Client>,
    tick_delay: u32,
    muxer: Muxer,
    demuxer: MpegTsDemuxer,
    audio_start: Option<DateTime<Utc>>,
}

impl EventListener {
    pub fn new(event_receiver: Receiver<Event>, event_sender: Sender<Event>, tick_delay: u32) -> EventListener {
        EventListener {
            event_receiver: event_receiver,
            event_sender: event_sender,
            clients: HashMap::new(),
            tick_delay: tick_delay,
            muxer: Muxer::new(),
            demuxer: MpegTsDemuxer::new(),
            audio_start: None,
        }
    }
    
    pub fn run(mut self) {
        let event_sender = self.event_sender.clone();
        let mut shared_state = SharedState::new(self.event_sender, self.tick_delay);
        loop {
            let event = self.event_receiver.recv().expect("Event channel closed (should not happen)");
            match event {
                Event::HeartBeat => {
                    for client in self.clients.iter_mut() {
                        if !client.1.presenter {
                            client.1.flush_json();
                        }
                    }
                    if shared_state.is_buffer_empty() {
                        shared_state.broadcast_json(&JSONMessage::heartbeat { ts: now() });
                    }
                    shared_state.flush_json();
                },
                Event::ClientBin(id, bin) => {
                    match self.clients.get_mut(&id) {
                        Some(client) => {
                            if client.closing {
                                continue;
                            }
                            if !client.presenter {
                                client.closing = true;
                                warn!("Client {} attempted to send audio without permission", id);
                                let _ = client.me.close(CloseCode::Error);
                            }
                            client.audio_source = true;
                        }
                        _ => {}
                    };

                    let muxer = &mut self.muxer;
                    let clients = &mut self.clients;
                    let audio_start = &mut self.audio_start;

                    let mut callback = |raw_frame: Vec<u8>| {
                        let is_first_cluster = muxer.is_first_cluster();
                        let is_cluster_start = muxer.is_cluster_start();
                        let is_cluster_end = muxer.is_cluster_end();
                        let computed_seek_ts = muxer.next_cluster_ts();
                        let is_stream_ended = raw_frame.len() == 0;

                        if !is_stream_ended {
                            muxer.process_frame(raw_frame);
                        }

                        if is_cluster_start {
                            let now = now();
                            if is_first_cluster {
                                *audio_start = now;
                            }
                            let seek_ts = (now.unwrap() - audio_start.unwrap()).num_milliseconds();
                            for client in clients.iter_mut() {
                                if !client.1.audio_active && !client.1.presenter {
                                    let json_msg = JSONMessage::audio {
                                        ts: now,
                                        seek_ts: Some(seek_ts as u64),
                                        reloc_offset: Some(muxer.reloc_offset()),
                                        reloc_size: Some(muxer.reloc_size())
                                    };
                                    // info!("New listener: measured seek_ts={}, computed seek_ts={}", seek_ts, computed_seek_ts);
                                    client.1.send_json(&json_msg);

                                    /* Ensure the "audio" json message is sent before the audio
                                     * header, because the client depends on it to compute the
                                     * header timestamp */
                                    client.1.flush_json();

                                    let mut header :Vec<u8> = Vec::new();
                                    header.push(protocol::BIN_WEBM);
                                    muxer.get_header(&mut header);
                                    let _ = client.1.me.send(Message::binary(header));
                                    client.1.audio_active = true;
                                }
                            }
                        } 

                        if is_cluster_end || is_stream_ended {
                            if is_first_cluster {
                                info!("Audio stream is beginning");
                            }
                            let muxed_cluster = muxer.take_cluster();
                            if muxed_cluster.len() > 0 {
                                let mut all_synced = true;
                                for client in clients.iter() {
                                    if !client.1.audio_active {
                                        all_synced = false;
                                        break;
                                    }
                                }
                                // info!("Sending cluster: computed_seek_ts={}", computed_seek_ts);
                                debug!("Sending audio cluster of {} bytes", muxed_cluster.len());
                                let mut bin_packet = Vec::new();
                                bin_packet.push(protocol::BIN_WEBM);
                                bin_packet.extend(muxed_cluster);
                                if all_synced {
                                    for stream in shared_state.all_viewers.iter_mut() {
                                        let _ = stream.broadcast(Message::binary(bin_packet.clone()));
                                    }
                                } else {
                                    for client in clients.iter() {
                                        if client.1.audio_active {
                                            let _ = client.1.me.send(Message::binary(bin_packet.clone()));
                                        }
                                    }
                                }
                            }
                            if is_stream_ended {
                                for client in clients.iter_mut() {
                                    client.1.audio_active = false;
                                }
                                let json_msg = JSONMessage::audio {
                                    ts: Some(audio_start.unwrap().checked_add_signed(Duration::milliseconds(computed_seek_ts as i64)).unwrap()),
                                    seek_ts: None,
                                    reloc_offset: None,
                                    reloc_size: None,
                                };
                                shared_state.broadcast_json_viewers(&json_msg);
                                *audio_start = None;
                            }
                        }
                    };

                    if bin.len() > 0 {
                        match self.demuxer.process_frame(&bin, callback) {
                            Err(e) => {
                                match self.clients.get_mut(&id) {
                                    Some(client) => {
                                        warn!("Closing audio client due to error: {:?}", e);
                                        client.closing = true;
                                        let _ = client.me.close(CloseCode::Error);
                                    },
                                    _ => {},
                                };
                            },
                            _ => {}
                        }
                    } else {
                        callback(Vec::new());
                        self.muxer.reset();
                        self.demuxer.reset();
                    }
                },
                Event::ViewServer(stream) => {
                    shared_state.all_viewers = Some(stream);
                },
                Event::PrezServer(stream) => {
                    shared_state.all_presenters = Some(stream);
                },
                Event::Client(id, mut json_msg) => {
                    let client = self.clients.get_mut(&id).unwrap();
                    if !client.closing {
                        match client.on_json(&mut shared_state, &mut json_msg) {
                            Err(e) => {
                                warn!("Closing client {} due to error: {:?}", id, e);
                                client.closing = true;
                                let _ = client.me.close(CloseCode::Error);
                            },
                            _ => {}
                        };
                    }
                },
                Event::Open(id, stream, forbid_presenter) => {

                    /* Prevent new client from receiving old data */
                    match shared_state.buffer.take(true) {
                        Some(msg) => {
                            for client in self.clients.iter_mut() {
                                let _ = client.1.me.send(msg.clone());
                            }
                        },
                        None => {}
                    }

                    info!("New client {} (presenter: {}) (count: {})", id, !forbid_presenter, self.clients.len());
                    self.clients.insert(id, Client::new(id, stream, forbid_presenter));
                    self.clients.get_mut(&id).unwrap().on_open(&mut shared_state);
                },
                Event::Close(id) => {
                    let client = self.clients.get_mut(&id).unwrap();
                    client.on_close(&mut shared_state);
                    if client.audio_source {
                        event_sender.send(Event::ClientBin(id, Vec::new())).unwrap(); // signal audiostream end
                        info!("Removing audio source {}", id);
                    }

                    info!("Bye client {} (count: {})", id, self.clients.len());
                    self.clients.remove(&id);
                },
                Event::Plugin(plug_msg) => {
                    let json_msg = JSONMessage::channel {
                        ts: now(),
                        id: plug_msg.chan_id as u32,
                        data: ChanMessage::data {
                            data: Data::action {
                                content: plug_msg.data.to_string()
                            }
                        },
                    };
                    if shared_state.chan_list.contains_key(&plug_msg.chan_id) {
                        shared_state.broadcast_json(&json_msg);
                    }
                },
            };
        }
    }
}

struct ClientForwarder {
    out: ws::Sender,
    event_sender: Sender<Event>,
    my_id: uuid::Uuid,
    forbid_presenter: bool,
}

impl ClientForwarder {
    fn try_on_message(&mut self, msg: Message) -> ws::Result<()> {
        match msg {
            Message::Binary(msg) => {
                self.event_sender.send(Event::ClientBin(self.my_id, msg)).unwrap();
                /*
                if self.demuxer.is_none() {
                    self.demuxer = Some(MpegTsDemuxer::new(self.event_sender.clone()));
                }
                self.demuxer.as_mut().unwrap().process_frame(msg.as_slice());
                */
                Ok(())
            },
            Message::Text(msg) => {
                match serde_json::from_str(&msg) as serde_json::Result<JSONMessage> {
                    Ok(json_msg) => {
                        self.event_sender.send(Event::Client(self.my_id, json_msg)).unwrap();
                        Ok(())
                    },
                    Err(e) => { Err(fail(format!("JSON parse {:?}", e))) }
                }
            },
        }

    }
}

impl ws::Handler for ClientForwarder {
    fn on_open(&mut self, _ : ws::Handshake) -> ws::Result<()> {
        self.event_sender.send(Event::Open(self.my_id, self.out.clone(), self.forbid_presenter)).unwrap();
        Ok(())
    }

    fn on_message(&mut self, msg: Message) -> ws::Result<()> {
        match self.try_on_message(msg) {
            Err(e) => {
                warn!("Kicking client {} due to error: {:?}", self.my_id, e);
                Err(e)
            },
            ok => {ok}
        }
    }

    fn on_close(&mut self, _code: CloseCode, _reason: &str) {
        self.event_sender.send(Event::Close(self.my_id)).unwrap();
    }

    fn on_error(&mut self, _err: ws::Error) {
        let _ = self.out.close(CloseCode::Error);
    }
    
}

/* This structure contains all shared state between all clients */
pub struct SockServ { 
    event_sender: Sender<Event>,
    url: String,
    forbid_presenter: bool,
} 

impl SockServ {
    pub fn new(event_sender: Sender<Event>, url: String, forbid_presenter: bool) -> SockServ {
        SockServ { event_sender: event_sender, url: url, forbid_presenter: forbid_presenter}
    }

    pub fn listen(&self) -> ws::Result<()> {
            let server_sock = Builder::new().with_settings(Settings{ max_connections: 10_000, ..Settings::default()})
            .build(|out:ws::Sender| {
                ClientForwarder { out: out, event_sender: self.event_sender.clone(), my_id: Uuid::new_v4(), forbid_presenter: self.forbid_presenter}
            })?;
            let broadcaster = server_sock.broadcaster();
            if self.forbid_presenter {
                self.event_sender.send(Event::ViewServer(broadcaster)).unwrap();
            } else {
                self.event_sender.send(Event::PrezServer(broadcaster)).unwrap();
            }
            server_sock.listen(&self.url)?;
            Ok(())
    }

    pub fn launch(self) {
        let event_sender = self.event_sender.clone();
        if !self.forbid_presenter {
            let _ = thread::spawn(move || {
                loop {
                    thread::sleep(time::Duration::from_millis(1000));
                    event_sender.send(Event::HeartBeat).unwrap();
                }
            });
        }
        let _ = thread::spawn(move || {
            match self.listen() {
                Err(e) => {
                    error!("Failed to bind to {} ({})", self.url, e.to_string());
                    exit(1);
                },
                _ => {}
            };
        });
    }
}

