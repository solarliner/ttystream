# What is it? 

This is a tool to do remote presentations. It supports PDF slides (with pointer sharing), and terminal (text) sharing, and an optional real-time audio stream. 

It is very lightweight. The bandwidth used (per client) never exceeds 10kB/s, and is usually around 5kB/s. The server resource usage is also very light: benchmarks performed on a Raspberry-PI 3 have shown that more than 5000 clients can be served concurrently, without any noticeable performance degradation.

It is composed of a backend (websocket server), a front-end (hostable using your favorite web server), a TTY caster tool (to share your terminal), and an audio sender tool (to stream real-time audio).

If you already have a server (for example, provided by your network administrator), you can skip the "Server setup" part.

# Server setup

## Prerequisites

 * (Optional) Install the Rust toolchain (https://rustup.rs/)

 * (Optional) Install the Nodejs toolchain (https://nodejs.org/en/download/)

 * Install a web server (this documentation assumes that you installed Nginx)

## Websocket server compilation and installation

### Option 1: Building the websocker server (requires the Rust toolchain)

**Security advice:** It is recommended that you create a dedicated user for the websocket server (do not run it as root).

From the project root directory, type:

```
cargo install --path server
```

The server executable will be installed in `$HOME/.cargo/bin/`

### Option 2: Downloading pre-built binary

You can also download a pre-built binary at `https://klemm.7un.net/presentation/server`:

```
wget https://klemm.7un.net/presentation/server
chmod +x server
```
Then copy the server binary in the directory of your choice (ex: `/usr/local/bin/`)

## Front-end compilation and installation

### Option 1: Building the front-end (requires the Nodejs toolchain)

Go to the directory containing the front-end source, and type:

```
npm ci
npm build 
```

Then, copy the content of the `build` folder into a folder in your webserver's root:
```
mkdir /var/www/html/presentation/
cp -a build/* /var/www/html/presentation/
```

Also create directory `/var/www/html/presentation/static/` and copy lzma-min.js and lzma_worker-min.js from `https://github.com/LZMA-JS/LZMA-JS/tree/master/src` to `/var/www/html/presentation/static/`
 
We will assume that it will be accessible from `http://example.com/presentation`

### Option 2: Downloading pre-built front-end

Download https://klemm.7un.net/presentation/front.tgz unpack it. Then, copy the
result into your webserver's root.

```
wget https://klemm.7un.net/presentation/front.tgz
tar xzf front.tgz
mkdir /var/www/html/presentation
cp -a presentation/* /var/www/html/presentation/
```

Assuming that `/var/www/html` is the web-root for site `example.com`, the
front-end will be accessible from `http://example.com/presentation`

## Webserver configuration (nginx)

**Security advice:** It is recommended that you enable SSL on your webserver, otherwise sensitive information will be transmitted in plaintext.

Create an authentication file with an user (we will use `some_user` in this example)

```
htpasswd -c /etc/nginx/htpasswd some_user
```

Add the following snippet to your site configuration (by default `/etc/nginx/sites-available/default` or nginx, or `/etc/nginx/sites-available/default-ssl` if you have SSL enabled):

```
location /channels {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:4000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 7200;
}

location /preschannels {
        auth_basic "Presenter only";
        auth_basic_user_file /etc/nginx/htpasswd;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header Host $host;
        proxy_pass http://127.0.0.1:4001;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_read_timeout 7200;
}
```

Reload the nginx configuration:

```
nginx -s reload
```

## Usage

This examples makes the websocket server listen only on local interface (that is recommended), and assumes that your nginx server is on the same host.

Launch websocket server:
```
server -a 127.0.0.1:4000 -p 127.0.0.1:4001
```

Visit http://example.com/presentation join the presentation as a viewer.

Visit http://example.com/presentation?presenter=1 to join the presentation as a presenter (this will ask for authentication)

As presenter, you can open and close PDF documents, switch the active document, and share the location of your mouse pointer. Opening shared terminals require the TTY caster program described below.

Holding the `c` hotkey projects your mouse pointer as a red "laser pointer" dot.

Because of CORS restrictions, when opening a PDF, it must be hosted on the same website as your presentation front-end (i.e: http://example.com/doc/Lesson1.pdf is OK, but http://whatever.com/stuff.pdf is not)

**Security advice:** If there are local users on the same host as the server, they will be able to connect to the presenter socket directly (bypassing nginx authentication).

## Manage websocket server using systemd (optional) 

Let's assume that you created a dedicated Unix user `unixuser` (in group
`unixgroup`) and that your `server` binary is in folder
`/home/unixuser/.cargo/bin/`.

Create the file /etc/systemd/system/ttystream.service with the following:

```
[Unit]
Description= TTYStream Websocket Server

[Service]
ExecStart=/home/unixuser/.cargo/bin/server  -a 127.0.0.1:4000 -p 127.0.0.1:4001 -l info
Type=simple
User=unixuser
Group=unixgroup

[Install]
WantedBy=multi-user.target
```

Do not forget to remplace `unixuser`, `unixgroup` and `/home/unixuser/.cargo/bin/` respectively by the name of your dedicated unix user, dedicated unix group, and server binary location. You can also replace `info` by `debug` or `warn` to adjust the log-level.

Reload systemd by typing:

`systemctl daemon-reload`

Type this to start websocket server:

`systemctl start ttystream.service`

Type this to stop websocket server:

`systemctl stop ttystream.service`

Type this to launch automatically at boot:

`systemctl enable ttystream.service`

Type this to disable auto-launch at boot:

`systemctl disable ttystream.service`

# TTY caster setup

The TTY caster is a tool to share your terminal. It must be installed locally on the presenter's machine. It is required only if you want to do terminal sharing.

## Compilation and installation

### Option 1: Building the TTY caster (requires Rust toolchain)

From the project root directory, type:

```
cargo install --path caster 
```

### Option 2: Downloading pre-built binary

You can also download a pre-built binary at `https://klemm.7un.net/presentation/caster`:

```
wget https://klemm.7un.net/presentation/caster
chmod +x caster 
```

Then copy the caster binary in the directory of your choice (ex: `/usr/local/bin/`)

## Usage

Cast a terminal by doing:

```
caster -u ws://example.com/preschannels/ -U some_user
```

This will create a terminal on the web front-end, mirroring the content of your local terminal. If the web front-end is currently showing another document (such as a PDF), it will not show your new terminal directly, but it will appear in the drop down list of showable documents in your presenter web front-end. 

If you have SSL on your web server (recommended), use `wss://` instead of `ws://`

# Audio sender setup 

The audio sender tool is used to upload a real-time audio stream. 

## Compilation and installation

### Option 1: Building the audio sender (requires Rust toolchain)

From the project root directory, type:

```
cargo install --path audiosend 
```

### Option 2: Downloading pre-built binary

You can also download a pre-built binary at `https://klemm.7un.net/presentation/audiosend`:

```
wget https://klemm.7un.net/presentation/audiosend
chmod +x audiosend
```

Then copy the audiosend binary in the directory of your choice (ex: `/usr/local/bin/`)

## Usage

The usage of `audiosend` requires that `ffmpeg` is installed and present in your `$PATH`.

Stream an audio file by doing:

```
audiosend -u ws://example.com/preschannels/ -i somefile.ogg -U some_user
```

Stream your default PulseAudio recording device (e.g. microphone):

```
audiosend -u ws://example.com/preschannels/ -f pulse -i default -U some_user
```

Various flags exists (try `audiosend --help`) to control the encoding (e.g.) bitrate).

If you have SSL on your web server (recommended), use `wss://` instead of `ws://`

