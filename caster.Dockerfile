FROM rust:1-slim-stretch AS builder

WORKDIR /usr/src/ttystream

RUN apt-get update && \
    apt-get install -y pkg-config libssl-dev

COPY protocol/Cargo.toml protocol/Cargo.toml
COPY caster/Cargo.toml caster/Cargo.toml

RUN cd protocol && \
    mkdir src && \
    touch src/main.rs && \
    cd ../caster && \
    mkdir src && \
    touch src/main.rs && \
    cargo fetch

COPY protocol protocol
COPY caster caster

RUN cd caster && \
    cargo build --release

FROM debian:stretch-slim

RUN apt-get update && \
    apt-get install -y libssl-dev

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/ttystream/caster/target/release/caster /usr/local/bin/caster

ENV server=ws://ws:80

CMD ["caster", "--url", ${server} ]
