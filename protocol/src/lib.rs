use serde::{Serialize, Deserialize};
use chrono::{DateTime, Utc};

pub const BIN_WEBM: u8 = 0x01;
pub const BIN_LZMA: u8 = 0x02;

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "chanType", content = "init")]
pub enum ChanInitData {
    tty {
        cols: u8,
        rows: u8,
        remote: String,
    },
    pdf {
        file: String,
        initial_page: u32,
    },
    chat {
    },
}

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "op")]
pub enum CtrlMessage {
    create {
        scene: u32,
        id: Option<u32>,
        #[serde(flatten)]
        init: ChanInitData,
    },
    delete { 
        id: u32,
        reason: Option<String>,
    },
    r#move {
        id: u32,
        scene: u32,
    },
}

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type")]
pub enum Data {
    action {
        content: String,
    },
    resize {
        cols: u8,
        rows: u8,
    },
    page {
        page: u32,
    },
    chat {
        sender: String,
        msg: String,
    },
}

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "subtype")]
pub enum ChanMessage {
    data {
        data: Data
    },
    metadata {
        name: String,
        value: String,
    },
}

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum PointerOp {
    on {
        x: f32,
        y:f32,
    },
    off {
        disabled: bool,
    },
}

#[allow(non_snake_case,non_camel_case_types)]
#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "type")]
pub enum JSONMessage {
    hello {
        presenter: bool
    },
    control {
        ts: Option<DateTime<Utc> >,
        #[serde(flatten)]
        ctl: CtrlMessage,
    },
    focus {
        ts: Option<DateTime<Utc> >,
        id: Option<u32>,
    },
    channel {
        ts: Option<DateTime<Utc> >,
        id: u32,
        #[serde(flatten)]
        data: ChanMessage,
    },
    pointer {
        ts: Option<DateTime<Utc> >,
        #[serde(flatten)]
        ptr_op: PointerOp,
    },
    globalmetadata {
        ts: Option<DateTime<Utc> >,
        name: String,
        value: String,
    },
    created {
        ts: Option<DateTime<Utc> >,
        id: u32,
    },
    audio {
        ts: Option<DateTime<Utc> >,
        seek_ts: Option<u64>,
        reloc_offset: Option<usize>,
        reloc_size: Option<usize>,
    },
    sync {
        ts: Option<DateTime<Utc> >,
    },
    initdone {
        ts: Option<DateTime<Utc> >,
    },
    heartbeat {
        ts: Option<DateTime<Utc> >,
    },
}
