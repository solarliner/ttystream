FROM rust:1-slim-stretch AS builder

WORKDIR /usr/src/ttystream

COPY protocol/Cargo.toml protocol/Cargo.toml
COPY server/Cargo.toml server/Cargo.toml

RUN cd protocol && \
    mkdir src && \
    touch src/main.rs && \
    cd ../server && \
    mkdir src && \
    touch src/main.rs && \
    cargo fetch

COPY protocol protocol
COPY server server

RUN cd server && \
    cargo build --release

FROM debian:stretch-slim

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/ttystream/server/target/release/server /usr/local/bin/ttystream

EXPOSE 80
EXPOSE 81

CMD ["ttystream", "--addr", "0.0.0.0:80", "--presenter", "0.0.0.0:81", "--log-level", "warn" ]
